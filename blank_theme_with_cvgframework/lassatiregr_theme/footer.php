<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package LassatireGr_Theme
 */

?>
	<div class="footer-menu">
		<?php
			$menu_name = 'footer_menu_'.ICL_LANGUAGE_CODE;
			$extra_items  = '
			';
			$footer_menu = array(
				'theme_location'  => '',
				'menu'            => $menu_name,
				'container'       => '',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'footer-menu',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				//%1$s is expanded to the value of the 'menu_id' parameter, %2$s is expanded to the value of the 'menu_class' parameter, and %3$s is expanded to the value of the list items
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s'.$extra_items.'</ul>',
				'depth'           => 0,
				'walker'          => ''
			);
			wp_nav_menu( $footer_menu );
		?>
	</div>

<?php wp_footer(); ?>

</body>
</html>
