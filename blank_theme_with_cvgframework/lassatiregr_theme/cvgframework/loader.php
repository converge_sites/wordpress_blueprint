<?php
	define('CVGFRAMEWORK_PATH', get_template_directory() . '/cvgframework/');
	define('CVGFRAMEWORK_URI', get_template_directory_uri(). '/cvgframework/');
	
	require_once CVGFRAMEWORK_PATH.'assets/assets.php';
	require_once CVGFRAMEWORK_PATH.'cpt/cpt.php';
	require_once CVGFRAMEWORK_PATH.'helpers/helpers.php';
	//require_once CVGFRAMEWORK_PATH.'metaboxes/metaboxes.php';
	//require_once CVGFRAMEWORK_PATH.'options/options-loader.php';
	require_once CVGFRAMEWORK_PATH.'shortcodes/theme_shortcode.php';
