<?php
/* **************************************************************************************
*****************************************************************************************
*****************************************************************************************
                    CUSTOM POST TYPES AND TAXONOMIES FUNCTIONS
**************************************************************************************
**************************************************************************************
************************************************************************************** */

    /* **************************************************************************************
	CUSTOM POST TYPES
	**************************************************************************************  */
	if(!function_exists('cvgframework_register_post_types')) {
	    function cvgframework_register_post_types() {
			register_post_type( 'site-settings', array( 'labels' 				=> array('name' => __("Site Settings", get_theme_text_domain())),
														'public' 				=> false,
														'show_ui'            	=> true,
														'show_in_menu'        	=> true,
														'show_in_nav_menus'   	=> true,
														'has_archive'		    => false,
														'hierarchical'          => true,
														'supports'				=> array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
			));
			/*
				register_post_type( 'actions', array(	'labels' 				=> array('name' => __("Actions", get_theme_text_domain())),
														'public' 				=> true,
														'show_ui'            	=> true,
														'show_in_menu'        	=> true,
														'show_in_nav_menus'   	=> true,
														'has_archive'		   	=> true,
														'hierarchical'         	=> true,
														'supports'				=> array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
				));
			
				//flush_rewrite_rules();
			*/
	    }
	    add_action( 'init', 'cvgframework_register_post_types' );
      }

    /* **************************************************************************************
	CUSTOM TAXONOMIES
	************************************************************************************** */
    if(!function_exists('cvgframework_define_taxonomies')) {
        function cvgframework_define_taxonomies() {
			/*
			register_taxonomy( 'actions-category', 'actions', array(	'hierarchical'  => true,
																		'label'         => 'Actions Category',
																		'query_var'     => true,
																		'rewrite'       => true
            ));
            //flush_rewrite_rules();
            */
        }
    }
	add_action( 'init', 'cvgframework_define_taxonomies' );

    /* **************************************************************************************
	GENERAL IN CUSTOM POST TYPES
	**************************************************************************************
    if(!function_exists('custom_flush_rules')) {
        function custom_flush_rules(){
            flush_rewrite_rules();
        }
        add_action('after_theme_switch', 'custom_flush_rules');
    }
    */
?>
