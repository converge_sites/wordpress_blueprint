<?php
	/* ************************************************************************************** 
	Require MetaboxIO initialazation
	************************************************************************************** */		
	require_once 'metaboxio_4_5_3/meta-box.php';
	
	/* ************************************************************************************** 
	Register Custom Metaboxes
	************************************************************************************** */		
      if(!function_exists('cvgframework_register_meta_boxes')) {
	  /**
		  * Register meta boxes
		  *
		  * Remember to change "your_prefix" to actual prefix in your project
		  *
		  * @param array $meta_boxes List of meta boxes
		  *
		  * @return array
		  */
	      function cvgframework_register_meta_boxes( $meta_boxes ){	
	      
		  //SEO general box
		      $metabox_prefix = get_the_prefix('metabox_', 'seo_general_box_'); // prefix of meta keys (optional). Use underscore (_) at the beginning to make keys hidden. Alt.: You also can make prefix empty to disable it. Better has an underscore as last sign
				$meta_boxes[] = array(				
				'id'         => "{$metabox_prefix}metabox", // Meta box id, UNIQUE per meta box. Optional since 4.1.5
				'title'      => __( 'SEO General', get_theme_text_domain()), // Meta box title - Will appear at the drag and drop handle bar. Required.
				'post_types' => array( 'post', 'page'), // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
				'context'    => 'normal', // Where the meta box appear: normal (default), advanced, side. Optional.
				'priority'   => 'low', // Order of meta box: high (default), low. Optional.
				'autosave'   => false, // Auto save: true, false (default). Optional.                   
				'fields'     => array(                        
					array(                                 
					'id'    => "{$metabox_prefix}description",
					'name'  => __( 'SEO description', get_theme_text_domain() ),
					'desc'  => __( '150 chars MAX', get_theme_text_domain() ),
					'type'  => 'text', 
					'clone' => false,
					),
					array( 
					'id'    => "{$metabox_prefix}keywords",
					'name'  => __( 'SEO keywords', get_theme_text_domain() ),
					'type'  => 'text', 
					'clone' => true,
					),					
				),
		      );
		 // Site Settings CPT METABOXES 
		      //Site Settings Features box
				$metabox_prefix = get_the_prefix('metabox_', 'site_settings_general_box_'); // prefix of meta keys (optional). Use underscore (_) at the beginning to make keys hidden. Alt.: You also can make prefix empty to disable it. Better has an underscore as last sign
				$meta_boxes[] = array(
					'id'         => "{$metabox_prefix}metabox", // Meta box id, UNIQUE per meta box. Optional since 4.1.5
					'title'      => __( 'Site Settings General', get_theme_text_domain()), // Meta box title - Will appear at the drag and drop handle bar. Required.
					'post_types' => array( 'site-settings' ), // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
					'context'    => 'normal', // Where the meta box appear: normal (default), advanced, side. Optional.
					'priority'   => 'high', // Order of meta box: high (default), low. Optional.
					'autosave'   => false, // Auto save: true, false (default). Optional.                   
					'fields'     => array(
						array( 
							'id'    => "{$metabox_prefix}facebook_link",
							'name'  => __( 'facebook_link', get_theme_text_domain() ),
							'type'  => 'text', 
							'clone' => false,
						),
						array(                                 
							'id'    => "{$metabox_prefix}twitter_link",
							'name'  => __( 'twitter_link', get_theme_text_domain() ),
							'type'  => 'text', 
							'clone' => false,
						),
						array(                                 
							'id'    => "{$metabox_prefix}youtube_link",
							'name'  => __( 'youtube_link', get_theme_text_domain() ),
							'type'  => 'text', 
							'clone' => false,
						),
						
					),			      

				);
				$metabox_prefix = get_the_prefix('metabox_', 'site_settings_header_box_'); // prefix of meta keys (optional). Use underscore (_) at the beginning to make keys hidden. Alt.: You also can make prefix empty to disable it. Better has an underscore as last sign
				$meta_boxes[] = array(
					'id'         => "{$metabox_prefix}metabox", // Meta box id, UNIQUE per meta box. Optional since 4.1.5
					'title'      => __( 'Site Settings Header', get_theme_text_domain()), // Meta box title - Will appear at the drag and drop handle bar. Required.
					'post_types' => array( 'site-settings' ), // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
					'context'    => 'normal', // Where the meta box appear: normal (default), advanced, side. Optional.
					'priority'   => 'high', // Order of meta box: high (default), low. Optional.
					'autosave'   => false, // Auto save: true, false (default). Optional.                   
					'fields'     => array(
						array( 
							'id'    => "{$metabox_prefix}logo_header",
							'name'  => __( 'logo_header', get_theme_text_domain() ),
							'type'  => 'image_advanced',
							'max_file_uploads' => 1,
						),						
						array( 
							'id'    => "{$metabox_prefix}image_right",
							'name'  => __( 'image_right', get_theme_text_domain() ),
							'type'  => 'image_advanced',
							'max_file_uploads' => 1,
						),						
						
					),			      

				);
				$metabox_prefix = get_the_prefix('metabox_', 'site_settings_footer_box_'); // prefix of meta keys (optional). Use underscore (_) at the beginning to make keys hidden. Alt.: You also can make prefix empty to disable it. Better has an underscore as last sign
				$meta_boxes[] = array(
					'id'         => "{$metabox_prefix}metabox", // Meta box id, UNIQUE per meta box. Optional since 4.1.5
					'title'      => __( 'Site Settings Footer', get_theme_text_domain()), // Meta box title - Will appear at the drag and drop handle bar. Required.
					'post_types' => array( 'site-settings' ), // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
					'context'    => 'normal', // Where the meta box appear: normal (default), advanced, side. Optional.
					'priority'   => 'high', // Order of meta box: high (default), low. Optional.
					'autosave'   => false, // Auto save: true, false (default). Optional.                   
					'fields'     => array(
						array( 
							'id'    => "{$metabox_prefix}logo_footer",
							'name'  => __( 'logo_footer', get_theme_text_domain() ),
							'type'  => 'image_advanced',
							'max_file_uploads' => 1,
						),
						array( 
							'id'    => "{$metabox_prefix}logo_footer_subtitle",
							'name'  => __( 'logo_footer_subtitle', get_theme_text_domain() ),
							'type'  => 'text',
							'clone' => false,
						),
						array( 
							'id'    => "{$metabox_prefix}copyright_footer",
							'name'  => __( 'copyright_footer', get_theme_text_domain() ),
							'type'  => 'text',
							'clone' => false,
						),
						array( 
							'id'    => "{$metabox_prefix}converge_logo_footer",
							'name'  => __( 'converge_logo_footer', get_theme_text_domain() ),
							'type'  => 'image_advanced',
							'max_file_uploads' => 1,
						),
						array( 
							'id'    => "{$metabox_prefix}links_images",
							'name'  => __( 'links_images', get_theme_text_domain() ),
							'type'  => 'image_advanced',
							
						),
					),			      

				);
				
				$metabox_prefix = get_the_prefix('metabox_', 'site_settings_inititatives_box_'); // prefix of meta keys (optional). Use underscore (_) at the beginning to make keys hidden. Alt.: You also can make prefix empty to disable it. Better has an underscore as last sign
				$meta_boxes[] = array(
					'id'         => "{$metabox_prefix}metabox", // Meta box id, UNIQUE per meta box. Optional since 4.1.5
					'title'      => __( 'Site Settings Initiatives', get_theme_text_domain()), // Meta box title - Will appear at the drag and drop handle bar. Required.
					'post_types' => array( 'site-settings' ), // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
					'context'    => 'normal', // Where the meta box appear: normal (default), advanced, side. Optional.
					'priority'   => 'high', // Order of meta box: high (default), low. Optional.
					'autosave'   => false, // Auto save: true, false (default). Optional.                   
					'fields'     => array(
						array( 
							'id'    => "{$metabox_prefix}image_healthcare",
							'name'  => __( 'image_healthcare', get_theme_text_domain() ),
							'type'  => 'image_advanced',
							'max_file_uploads' => 1,
						),	
						array( 
							'id'    => "{$metabox_prefix}image_environment",
							'name'  => __( 'image_environment', get_theme_text_domain() ),
							'type'  => 'image_advanced',
							'max_file_uploads' => 1,
						),	
						array( 
							'id'    => "{$metabox_prefix}image_civil-society",
							'name'  => __( 'image_civil-society', get_theme_text_domain() ),
							'type'  => 'image_advanced',
							'max_file_uploads' => 1,
						),	
						array( 
							'id'    => "{$metabox_prefix}image_education",
							'name'  => __( 'image_education', get_theme_text_domain() ),
							'type'  => 'image_advanced',
							'max_file_uploads' => 1,
						),	
						
					),			      

				);
				
			  
			  
			  
		  // ACTIONS CPT METABOXES 
		      //Actions Features box
			  $metabox_prefix = get_the_prefix('metabox_', 'actions_features_box_'); // prefix of meta keys (optional). Use underscore (_) at the beginning to make keys hidden. Alt.: You also can make prefix empty to disable it. Better has an underscore as last sign
			  $meta_boxes[] = array(				
			      'id'         => "{$metabox_prefix}metabox", // Meta box id, UNIQUE per meta box. Optional since 4.1.5
			      'title'      => __( 'Actions Features', get_theme_text_domain()), // Meta box title - Will appear at the drag and drop handle bar. Required.
			      'post_types' => array( 'actions' ), // Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
			      'context'    => 'normal', // Where the meta box appear: normal (default), advanced, side. Optional.
			      'priority'   => 'high', // Order of meta box: high (default), low. Optional.
			      'autosave'   => false, // Auto save: true, false (default). Optional.                   
			      'fields'     => array(						
						array( 
							'id'    => "{$metabox_prefix}year_of_grant",
							'name'  => __( 'year_of_grant', get_theme_text_domain() ),
							'type' => 'date',
							// jQuery date picker options. See here http://jqueryui.com/demos/datepicker
							'js_options' => array(
								'appendText'      => __( '(yyyy-mm-dd)', get_theme_text_domain() ),
								'autoSize'        => true,
								'buttonText'      => __( 'Select Date', get_theme_text_domain() ),
								'dateFormat'      => __( 'yy-mm-dd', get_theme_text_domain() ),
								'numberOfMonths'  => 2,
								'showButtonPanel' => true,
							),                         
						),						
						array(                                 
							'id'    => "{$metabox_prefix}place_of_grant",
							'name'  => __( 'place_of_grant', get_theme_text_domain() ),
							'desc'  => __( 'Place of grant', get_theme_text_domain() ),
							'type'  => 'text', 
							'clone' => false,
						),
						array(                                 
							'id'    => "{$metabox_prefix}recipient_description",
							'name'  => __( 'recipient_description', get_theme_text_domain() ),
							'desc'  => __( 'Recipient Description', get_theme_text_domain() ),
							'type'  => 'textarea', 
							'clone' => false,
						),
						array(                                 
							'id'    => "{$metabox_prefix}grant_description",
							'name'  => __( 'grant_description', get_theme_text_domain() ),
							'desc'  => __( 'Grant Description', get_theme_text_domain() ),
							'type'  => 'textarea', 
							'clone' => false,
						),
						array(                                 
							'id'    => "{$metabox_prefix}site_of_donation",
							'name'  => __( 'site_of_donation', get_theme_text_domain() ),
							'desc'  => __( 'full site path (e.g http://in.gr)', get_theme_text_domain() ),
							'type'  => 'text', 
							'clone' => false,
						),
						array(                                 
							'id'    => "{$metabox_prefix}category",
							'name'  => __( 'category', get_theme_text_domain() ),
							'desc'  => __( 'Category', get_theme_text_domain() ),
							'type'  => 'text', 
							'clone' => false,
						),

						
		
				),
			      
			  
			  );                    
	      return $meta_boxes;
		  }
	  add_filter( 'rwmb_meta_boxes', 'cvgframework_register_meta_boxes' );
      }