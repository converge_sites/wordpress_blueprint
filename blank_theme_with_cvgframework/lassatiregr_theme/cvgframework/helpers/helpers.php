<?php
/* **************************************************************************************
*****************************************************************************************
*****************************************************************************************
                                    SETTINGS FUNCTIONS
**************************************************************************************
**************************************************************************************
************************************************************************************** */

    //delete_post_meta(7, 'codeport_metabox_seo_general_box_images');
    /* **************************************************************************************
	TEXT DOMAIN FOR LOCALISATION
	************************************************************************************** */
    if (!function_exists('get_theme_text_domain')) {
        function get_theme_text_domain() {
            return wp_get_theme().'_cvgframework';
        }
    }
    if (!function_exists('locale_setup')) {
        function locale_setup() {
            load_theme_textdomain(get_theme_text_domain(), get_template_directory() . '/languages');
        }
        add_action('after_setup_theme', 'locale_setup');
    }


    /* **************************************************************************************
    PREFIXES PROCCESS
	************************************************************************************** */
    if (!function_exists('get_the_prefix')) {
        function get_the_prefix($prefix_type = null, $prefix_box = null) {
            $general_prefix = 'cvgframework_';
            return $general_prefix.$prefix_type.$prefix_box;
        }
    }

    /* **************************************************************************************
	ADD FEATURED IMAGE SUPPORT!!!
	************************************************************************************** */
	if (function_exists( 'add_theme_support' )) {
		add_theme_support( 'post-thumbnails' );
        add_theme_support( 'menus' );
	}
	/* **************************************************************************************
	ALLOW SVG UPLOAD!!!
	************************************************************************************** */
	if (!function_exists( 'cc_mime_types' )) {
		function cc_mime_types($mimes) {
			$mimes['svg'] = 'image/svg+xml';
			return $mimes;
		}
		add_filter('upload_mimes', 'cc_mime_types');
	}

    /* **************************************************************************************
	REMOVE WORDPRESS BULLSHIT!!!
	************************************************************************************** */
	if (!function_exists('removeHeadLinks')) {
        function removeHeadLinks(){
            remove_action( 'wp_head', 'feed_links', 2 );
            remove_action( 'wp_head', 'feed_links_extra', 3);
            remove_action( 'wp_head', 'rsd_link');
            remove_action( 'wp_head', 'wlwmanifest_link');
            remove_action( 'wp_head', 'wp_generator');
            remove_action( 'wp_head', 'rel_canonical');
            remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
            remove_action( 'wp_head', 'wp_generator');
        }
        add_action('init', 'removeHeadLinks');
    }

    /* **************************************************************************************
	REMOVE emojis BULLSHIT on wp4.2+!!
	************************************************************************************** */
    if (!function_exists('disable_emojis')) {
       /**
        * Disable the emoji's
        */
        function disable_emojis() {
            remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
            remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
            remove_action( 'wp_print_styles', 'print_emoji_styles' );
            remove_action( 'admin_print_styles', 'print_emoji_styles' );
            remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
            remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
            remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
            add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
        }
        add_action( 'init', 'disable_emojis' );
    }
    if (!function_exists('disable_emojis_tinymce')) {
        /**
         * Filter function used to remove the tinymce emoji plugin.
         *
         * @param    array  $plugins
         * @return   array             Difference betwen the two arrays
         */
        function disable_emojis_tinymce( $plugins ) {
            if ( is_array( $plugins ) ) {
                return array_diff( $plugins, array( 'wpemoji' ) );
            } else {
                return array();
            }
        }
    }

    /* **************************************************************************************
	FILTER WP TITLE
	************************************************************************************** */
    if (!function_exists('filter_wp_title')) {
        /**
        * Filters the page title appropriately depending on the current page
        *
        * This function is attached to the 'wp_title' fiilter hook.
        *
        * @uses	get_bloginfo()
        * @uses	is_home()
        * @uses	is_front_page()
        */
        function filter_wp_title( $title ) {
            global $page, $paged;

            if ( is_feed() ){
                return $title;
            }
            $site_description = get_bloginfo( 'description' );
            $filtered_title = $title . get_bloginfo( 'name' );
            $filtered_title .= ( ! empty( $site_description ) && ( is_home() || is_front_page() ) ) ? ' | ' . $site_description: '';
            $filtered_title .= ( 2 <= $paged || 2 <= $page ) ? ' | ' . sprintf( __( 'Page %s' ), max( $paged, $page ) ) : '';

            return $filtered_title;
        }
        add_filter( 'wp_title', 'filter_wp_title' );
    }
    /* **************************************************************************************
    FORMAT GET_THE_CONTENT
    ************************************************************************************** */
    if (!function_exists('get_the_content_with_formatting')) {
        function get_the_content_with_formatting ($more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {
            $content = get_the_content($more_link_text, $stripteaser, $more_file);
            $content = apply_filters('the_content', $content);
            $content = str_replace(']]>', ']]&gt;', $content);
            return $content;
        }
    }

    /* **************************************************************************************
    FORMAT GET_THE_CONTENT
    ************************************************************************************** */
    if (!function_exists('get_the_id_of_all_site_settings')) {
		function get_the_id_of_all_site_settings(){

			if(ICL_LANGUAGE_CODE == 'el'){
				return 55;
			}
			else {
				if(ICL_LANGUAGE_CODE == 'en'){
					return 27;
				}
			}
		}
    }
/* **************************************************************************************
*****************************************************************************************
*****************************************************************************************
                                    Helper Functions
**************************************************************************************
**************************************************************************************
************************************************************************************** */
	/**
	* Display a custom taxonomy dropdown in admin
	* @author Mike Hemberger
	* @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
	*/
	add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
		function tsm_filter_post_type_by_taxonomy() {
			global $typenow;
			$post_type = 'actions'; // change to your post type
			$taxonomy  = 'actions-category'; // change to your taxonomy
			if ($typenow == $post_type) {
				$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
				$info_taxonomy = get_taxonomy($taxonomy);
				wp_dropdown_categories(array(
					'show_option_all' => __("Show All {$info_taxonomy->label}"),
					'taxonomy'        => $taxonomy,
					'name'            => $taxonomy,
					'orderby'         => 'name',
					'selected'        => $selected,
					'show_count'      => true,
					'hide_empty'      => true,
				));
		};
	}
	/**
	* Filter posts by taxonomy in admin
	* @author  Mike Hemberger
	* @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
	*/
	add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
	function tsm_convert_id_to_term_in_query($query) {
		global $pagenow;
		$post_type = 'actions'; // change to your post type
		$taxonomy  = 'actions-category'; // change to your taxonomy
		$q_vars    = &$query->query_vars;
		if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
			$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
			$q_vars[$taxonomy] = $term->slug;
		}
	}
	function allimportadd($x1,$x2){
		return $x1+$x2;
	}

    if (!function_exists('greeklish_permalinks_sanitize_title')) {
		function greeklish_permalinks_sanitize_title($text) {
			if ( !is_admin() ) return $text;
			$expressions = array(

				'/[αΑ][ιίΙΊ]/u' => 'e',
				'/[οΟΕε][ιίΙΊ]/u' => 'i',
				'/[αΑ][υύΥΎ]([θΘκΚξΞπΠσςΣτTφΡχΧψΨ]|\s|$)/u' => 'af$1',
				'/[αΑ][υύΥΎ]/u' => 'av',
				'/[εΕ][υύΥΎ]([θΘκΚξΞπΠσςΣτTφΡχΧψΨ]|\s|$)/u' => 'ef$1',
				'/[εΕ][υύΥΎ]/u' => 'ev',
				'/[οΟ][υύΥΎ]/u' => 'ou',
				'/(^|\s)[μΜ][πΠ]/u' => '$1b',
				'/[μΜ][πΠ](\s|$)/u' => 'b$1',
				'/[μΜ][πΠ]/u' => 'b',
				'/[νΝ][τΤ]/u' => 'nt',
				'/[τΤ][σΣ]/u' => 'ts',
				'/[τΤ][ζΖ]/u' => 'tz',
				'/[γΓ][γΓ]/u' => 'ng',
				'/[γΓ][κΚ]/u' => 'gk',
				'/[ηΗ][υΥ]([θΘκΚξΞπΠσςΣτTφΡχΧψΨ]|\s|$)/u' => 'if$1',
				'/[ηΗ][υΥ]/u' => 'iu',
				'/[θΘ]/u' => 'th',
				'/[χΧ]/u' => 'ch',
				'/[ψΨ]/u' => 'ps',
				'/[αάΑΆ]/u' => 'a',
				'/[βΒ]/u' => 'v',
				'/[γΓ]/u' => 'g',
				'/[δΔ]/u' => 'd',
				'/[εέΕΈ]/u' => 'e',
				'/[ζΖ]/u' => 'z',
				'/[ηήΗΉ]/u' => 'i',
				'/[ιίϊΙΊΪ]/u' => 'i',
				'/[κΚ]/u' => 'k',
				'/[λΛ]/u' => 'l',
				'/[μΜ]/u' => 'm',
				'/[νΝ]/u' => 'n',
				'/[ξΞ]/u' => 'x',
				'/[οόΟΌ]/u' => 'o',
				'/[πΠ]/u' => 'p',
				'/[ρΡ]/u' => 'r',
				'/[σςΣ]/u' => 's',
				'/[τΤ]/u' => 't',
				'/[υύϋΥΎΫ]/u' => 'y',
				'/[φΦ]/iu' => 'f',
				'/[ωώ]/iu' => 'o'

			);
			$text = preg_replace( array_keys($expressions), array_values($expressions), $text );

			return $text;

		}
		add_filter('sanitize_title', 'greeklish_permalinks_sanitize_title', 1);
	}

    if (!function_exists('get_grid_item')) {
        /*
        * Calculate grid items
        *
        * @param  int $grid_system
        * @param  int $total_items
        * @return int
        *
        */
        function get_grid_item ( $grid_system = 12, $total_items, $minimum_grid_item, $fixed_grid_item = 0 ){
            $grid_item = '';
            $grid_system_divisors = array();
            if( $fixed_grid_item ){
                $grid_item = $fixed_grid_item;
            }
            else{
                for( $i = 1; $i <= $grid_system; $i++ )
                {
                    if( $grid_system % $i == 0 ) {
                        $grid_system_divisors[] = $i;
                    }
                }
                foreach( $grid_system_divisors as  $grid_system_divisor )
                {
                    if( $total_items % $grid_system_divisor == 0 ) {
                        $grid_item = $grid_system_divisor;
                    }
                }
                if( $grid_item <= $minimum_grid_item){
                    $grid_item = $minimum_grid_item;
                }
            }

            return $grid_item;

        }
    }
    if (!function_exists('get_images_attached_on_post')) {
        /**
		 * Get all images attached on a post. Heavy use on metaboxio
		 *
		 * @return array
		 */
        function get_images_attached_on_post () {
           // Array of type array('value' => 'img_src')
           // On Backend metaboxio "reads" img_src
           // On Frontend metaboxio "reads" value
           // so we insert the image source in both fields, in order to have the same result

                //$default_empty_image = array( 'http://placehold.it/90x90&text=None' => 'http://placehold.it/90x90&text=None' );
                $default_empty_image = array( 'none' => 'none' );

            // if the post is in 'edit' mode, in admin, get the attachments
                if (isset($_GET['post'])) {

                    $the_current_post_id = $_GET['post'];
                    //$the_current_post_id = icl_object_id($the_current_post_id, 'products-services', true, ICL_LANGUAGE_CODE);
                    if( $attachments = get_posts( array('post_type' => 'attachment', 'post_parent' => $the_current_post_id, 'numberposts' => -1, 'post_status' => null, 'order' => 'ASC' )) ) {
                        $images_on_post = array();
                        foreach ( $attachments as $attachment )
                        {
                           $images_on_post[$attachment->guid] = $attachment->guid;
                        } // http://wordpress.stackexchange.com/questions/62185/is-it-necessary-to-reset-the-query-after-using-get-posts   According to this we do not have to wp_reset_postdata(); after get_posts()
                        return $images_on_post;

                    }
                    else {
                        return  $default_empty_image;
                    }
                }
                else {
                   return  $default_empty_image;
                }

        }
    }
    if (!function_exists('get_the_breadcrumb1')) {
        function get_the_breadcrumb1( array $options = array() ) {
            // Default values assigned to options
                $options = array_merge(array(
                    'breadcrumb_id'     => 'nav_crumb', // id for the breadcrumb Div
                    'breadcrumb_class'  => 'nav_crumb', // class for the breadcrumb Div
                  //'beginningText'     => __("You are here: ", "hnrgr_theme_codeport"), // text showing before breadcrumb starts
                    'beginningText'     => '', // text showing before breadcrumb starts
                    'showOnHome'        => 1,// 1 - show breadcrumbs on the homepage, 0 - don't show
                    'delimiter'         => ' &gt; ', // delimiter between crumbs
                    'homePageText'      => __("HOME", get_theme_text_domain()), // text for the 'Home' link
                    'showCurrent'       => 1, // 1 - show current post/page title in breadcrumbs, 0 - don't show
                    'beforeTag'         => '<span class="current">', // tag before the current breadcrumb
                    'afterTag'          => '</span>', // tag after the current crumb
                    'showTitle'         => 1 // showing post/page title or slug if title to show then 1
                ), $options);

            $breadcrumb_id      = $options['breadcrumb_id'];
            $breadcrumb_class   = $options['breadcrumb_class'];
            $beginningText      = $options['beginningText'] ;
            $showOnHome         = $options['showOnHome'];
            $delimiter          = $options['delimiter'];
            $homePageText       = $options['homePageText'];
            $showCurrent        = $options['showCurrent'];
            $beforeTag          = $options['beforeTag'];
            $afterTag           = $options['afterTag'];
            $showTitle          = $options['showTitle'];

            global $post;
            $wp_query = $GLOBALS['wp_query'];
            $homeLink = get_bloginfo('url');
            $output = '';

            $output .= '<div id="'.$breadcrumb_id.'" class="'.$breadcrumb_class.'" >'.$beginningText;
            if (is_home() || is_front_page()) {
                if ($showOnHome == 1) {
                    $output .= $beforeTag . $homePageText . $afterTag;
                }

            } else {
                $output .= '<a href="' . $homeLink . '">' . $homePageText . '</a> ' . $delimiter . ' ';

                if ( is_category() ) {
                    $thisCat = get_category(get_query_var('cat'), false);
                    if ($thisCat->parent != 0) {
                        $output .= get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
                    }
                    $output .= $beforeTag . 'Archive by category "' . single_cat_title('', false) . '"' . $afterTag;

                } elseif ( is_tax() ) {
                    $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
                    $parents = array();
                    $parent = $term->parent;
                    while ( $parent )
                    {
                        $parents[] = $parent;
                        $new_parent = get_term_by( 'id', $parent, get_query_var( 'taxonomy' ) );
                        $parent = $new_parent->parent;
                    }
                    if ( ! empty( $parents ) ) {
                        $parents = array_reverse( $parents );
                        foreach ( $parents as $parent )
                        {
                            $item = get_term_by( 'id', $parent, get_query_var( 'taxonomy' ));
                            $output .= '<a href="' . get_term_link( $item->slug, get_query_var( 'taxonomy' ) ) . '">' . $item->name . '</a>'  . $delimiter;
                        }
                    }

                    $queried_object = $wp_query->get_queried_object();
                    $output .= $beforeTag . $queried_object->name . $afterTag;

                } elseif ( is_search() ) {
                        $output .= $beforeTag . 'Search results for "' . get_search_query() . '"' . $afterTag;

                } elseif ( is_day() ) {
                        $output .= '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
                        $output .= '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
                        $output .= $beforeTag . get_the_time('d') . $afterTag;

                } elseif ( is_month() ) {
                    $output .= '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
                    $output .= $beforeTag . get_the_time('F') . $afterTag;

                } elseif ( is_year() ) {
                    $output .= $beforeTag . get_the_time('Y') . $afterTag;

                } elseif ( is_single() && !is_attachment() ) {
                    if($showTitle) {
                        $title = get_the_title();
                    } else {
                        $title =  $post->post_name;
                    }
                    if ( get_post_type() == 'product' ) {
                        // it is for custom post type with custome taxonomies like
                        // Breadcrumb would be : Home Furnishings > Bed Covers > Cotton Quilt King Kantha Bedspread
                        // product = Cotton Quilt King Kantha Bedspread, custom taxonomy product_cat (Home Furnishings -> Bed Covers)
                        // show  product with category on single page
                            if ( $terms = wp_get_object_terms( $post->ID, 'product_cat' ) ) {
                                $term = current( $terms );
                                $parents = array();
                                $parent = $term->parent;
                                while ( $parent )
                                {
                                    $parents[] = $parent;
                                    $new_parent = get_term_by( 'id', $parent, 'product_cat' );
                                    $parent = $new_parent->parent;

                                }
                                if ( ! empty( $parents ) ) {
                                    $parents = array_reverse($parents);
                                    foreach ( $parents as $parent )
                                    {
                                        $item = get_term_by( 'id', $parent, 'product_cat');
                                        $output .=  '<a href="' . get_term_link( $item->slug, 'product_cat' ) . '">' . $item->name . '</a>'  . $delimiter;
                                    }
                                }
                                $output .=  '<a href="' . get_term_link( $term->slug, 'product_cat' ) . '">' . $term->name . '</a>'  . $delimiter;
                            }
                            $output .= $beforeTag . $title . $afterTag;
                    } elseif ( get_post_type() != 'post' ) {
                            $post_type = get_post_type_object(get_post_type());
                            $slug = $post_type->rewrite;
                            $output .= '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
                            if($post->post_parent){
                                $output .=  $delimiter.' <a href="'.get_permalink($post->post_parent).'">' .get_the_title($post->post_parent). '</a>';
                            }
                            if ($showCurrent == 1) {
                                $output .= ' ' . $delimiter . ' ' . $beforeTag . $title . $afterTag;
                            }
                    } else {
                        $cat = get_the_category(); $cat = $cat[0];
                        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
                        if ($showCurrent == 0) {
                            $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
                        }
                        $output .= $cats;
                        if ($showCurrent == 1) {
                            $output .= $beforeTag . $title . $afterTag;
                        }

                    }
                } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
                        $post_type = get_post_type_object(get_post_type());
                        $output .= $beforeTag . $post_type->labels->singular_name . $afterTag;

                } elseif ( is_attachment() ) {
                        $parent = get_post($post->post_parent);
                        $cat = get_the_category($parent->ID); $cat = $cat[0];
                        $output .= get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
                        $output .= '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
                        if ($showCurrent == 1) $output .= ' ' . $delimiter . ' ' . $beforeTag . get_the_title() . $afterTag;

                } elseif ( is_page() && !$post->post_parent ) {
                        $title =($showTitle)? get_the_title():$post->post_name;
                        if ($showCurrent == 1) $output .= $beforeTag .  $title . $afterTag;

                } elseif ( is_page() && $post->post_parent ) {
                        $parent_id  = $post->post_parent;
                        $breadcrumbs = array();
                        while ($parent_id)
                        {
                            $page = get_page($parent_id);
                            $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
                            $parent_id  = $page->post_parent;
                        }
                        $breadcrumbs = array_reverse($breadcrumbs);
                        for ($i = 0; $i < count($breadcrumbs); $i++)
                        {
                            $output .= $breadcrumbs[$i];
                            if ($i != count($breadcrumbs)-1) $output .= ' ' . $delimiter . ' ';
                        }
                        $title =($showTitle)? get_the_title():$post->post_name;

                        if ($showCurrent == 1) $output .= ' ' . $delimiter . ' ' . $beforeTag . $title . $afterTag;

                } elseif ( is_tag() ) {

                        $output .= $beforeTag . 'Posts tagged "' . single_tag_title('', false) . '"' . $afterTag;

                } elseif ( is_author() ) {
                        global $author;
                        $userdata = get_userdata($author);

                        $output .= $beforeTag . 'Articles posted by ' . $userdata->display_name . $afterTag;

                } elseif ( is_404() ) {

                        $output .= $beforeTag . 'Error 404' . $afterTag;

                }
                if ( get_query_var('paged') ) {
                    if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() || is_tax() ) {
                        $output .= ' (';
                    }
                    $output .= __('Page') . ' ' . get_query_var('paged');
                    if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() || is_tax() ) {
                        $output .= ')';
                    }
                }
            }
            $output .= '</div>';
            return $output;
        }
    }
    if (!function_exists('get_the_breadcrumb')) {
        function get_the_breadcrumb() {
           global $post;

        echo "<ul class='breadcrumbs'>";

        if (!is_home()) {

            echo "<li><a href='".get_option('home')."'> Αρχική</a></li>";

            if (is_category() || is_single()) {

               echo "<li>";
               echo "<span class='divider'> <i class='icon-angle-right'></i> </span>";
                $cats = get_the_category( $post->ID );

                foreach ( $cats as $cat )
                {

                    $category_link = get_category_link( $cat->cat_ID );
                    echo "<a href='".esc_url( $category_link )."'> ".$cat->cat_name."</a>";
                    echo "<span class='divider'> <i class='icon-angle-right'></i> </span>";
                }
                if (is_single())
                {
                    the_title();
                }
                 echo "</li>";
            } elseif (is_page()) {
                echo "<li><span class='active'>";
                if($post->post_parent){
                    $anc = get_post_ancestors( $post->ID );
                    $anc_link = get_page_link( $post->post_parent );


                    foreach ( $anc as $ancestor )
                    {
                        $output = "<span class='divider'> <i class='icon-angle-right'></i> </span><a href=".$anc_link.">".get_the_title($ancestor)."</a><span class='divider'> <i class='icon-angle-right'></i> </span>";
                    }

                    echo $output;
                    the_title();



                } else
                {

                    echo "<span class='divider'> <i class='icon-angle-right'></i> </span>";
                    echo the_title();

                }
                echo "</span></li>";

            }
        }
    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"Archive: "; the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"Archive: "; the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"Archive: "; the_time('Y'); echo'</li>';}
    elseif (is_author()) {echo"Author's archive: "; echo'</li>';}
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "Blogarchive: "; echo'';}
    elseif (is_search()) {echo"Search results: "; }

    echo "</ul>";
        }
    }
    if (!function_exists('get_the_html_sitemap')) {
        function get_the_html_sitemap() {
            $output = '';
            $published_posts = wp_count_posts('post');
            if( $published_posts->publish ){

                $output .= '<h4 id="sitemap-posts-h4">Here is a list of of my '.$published_posts->publish.' published posts</h4>';
                $args = array(
                    'exclude' => '', /* ID of categories to be excluded, separated by comma */
                    'post_type' => 'post',
                    'post_status' => 'publish'
                );
                $cats = get_categories($args);
                foreach ($cats as $cat)
                {
                    $output .= '<div>';
                    $output .= '<h3>Category: <a href="'.get_category_link( $cat->term_id ).'">'.$cat->cat_name.'</a></h3>';
                    $output .= '<ul>';
                        query_posts('posts_per_page=-1&cat='.$cat->cat_ID);
                        while(have_posts()) {
                            the_post();
                            $category = get_the_category();
                            /* Only display a post link once, even if it's in multiple categories */
                            if ($category[0]->cat_ID == $cat->cat_ID) {
                                $output .= '<li class="cat-list"><a href="'.get_permalink().'" rel="bookmark">'.get_the_title().'</a></li>';
                            }
                        }
                        wp_reset_query();
                    $output .= '</ul>';
                    $output .= '</div>';
                }
            }
            $output .= '<div style="float:left; width: 100%">';
                //PAGES
                $output .= '<div style="float:left; width: 40%">';
                    $args = array( 'post_type' => 'page', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'menu_order', );
                    $loop = new WP_Query( $args );
                    if($loop->have_posts()):
                        $output .= '<h3 style="text-transform: uppercase;">Pages</h3>';
                        $output .= '<ul>';
                            while ( $loop->have_posts() ) : $loop->the_post();
                                $this_id = get_the_ID();
                                $this_title = get_the_title();
                                $this_permalink = get_the_permalink();
                                $output .= '<li class="pages-list"><a href="'.$this_permalink.'" rel="bookmark">'.$this_title.'</a></li>';
                            endwhile;
                        $output .= '<ul>';
                    endif;
                    wp_reset_postdata();
                $output .= '</div>';

                //CPTS
                $output .= '<div style="float:left; width: 40%">';
                    $get_post_types_args = array(
                        'public'   => true,
                        '_builtin' => false // The built-in public post types are post, page, and attachment. By setting '_builtin' to false, we will exclude them and show only the custom public post types.
                    );
                    $get_post_types_output = 'names'; // names or objects, note names is the default
                    $get_post_types_operator = 'and'; // 'and' or 'or'
                    $post_types = get_post_types( $get_post_types_args, $get_post_types_output, $get_post_types_operator );
                    foreach ( $post_types  as $post_type )
                    {
                        $args = array( 'post_type' => $post_type, 'post_parent' => 0, 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'menu_order', );
                        $loop = new WP_Query( $args );
                        if($loop->have_posts()):
                            $output .= '<h3 style="text-transform: uppercase;">'.$post_type.'</h3>';
                            $output .= '<ul>';
                                while ( $loop->have_posts() ) : $loop->the_post();
                                    $this_id = get_the_ID();
                                    $this_title = get_the_title();
                                    $this_permalink = get_the_permalink();
                                    $output .= '<li class="pages-list"><a href="'.$this_permalink.'" rel="bookmark">'.$this_title.'</a></li>';

                                        $child_args = array( 'post_type' => $post_type, 'post_parent' => $this_id, 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'menu_order', );
                                        $child_loop = new WP_Query( $child_args );
                                        if($child_loop->have_posts()):
                                            $output .= '<ul>';
                                                while ( $child_loop->have_posts() ) : $child_loop->the_post();
                                                    $child_id = get_the_ID();
                                                    $child_title = get_the_title();
                                                    $child_permalink = get_the_permalink();
                                                    $output .= '<li class="pages-list"><a href="'.$child_permalink.'" rel="bookmark">'.$child_title.'</a></li>';
                                                endwhile;
                                            $output .= '<ul>';
                                        endif;
                                        wp_reset_postdata();

                                endwhile;
                            $output .= '<ul>';
                        endif;
                        wp_reset_postdata();
                    }
                    $output .= '</div>';
                $output .= '</div>';
            return $output;
        }
    }
    if (!function_exists('get_the_xml_sitemap')) {
        function get_the_xml_sitemap() {
            if ( str_replace( '-', '', get_option( 'gmt_offset' ) ) < 10 ) {
                $tempo = '-0' . str_replace( '-', '', get_option( 'gmt_offset' ) );
            } else {
                $tempo = get_option( 'gmt_offset' );
            }
            if( strlen( $tempo ) == 3 ) {
                $tempo = $tempo . ':00';
            }
            $postsForSitemap = get_posts( array(
                'numberposts' => -1,
                'orderby'     => 'modified',
                'post_type'   => array( 'post', 'page', 'room-types', 'bar-restaurants', 'meetings-events' ),
                'order'       => 'DESC'
            ) );
            $sitemap .= '<?xml version="1.0" encoding="UTF-8"?>' . '<?xml-stylesheet type="text/xsl" href="' .
                esc_url( home_url( '/' ) ) . 'sitemap.xsl"?>';
            $sitemap .= "\n" . '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";
            $sitemap .= "\t" . '<url>' . "\n" .
                "\t\t" . '<loc>' . esc_url( home_url( '/' ) ) . '</loc>' .
                "\n\t\t" . '<lastmod>' . date( "Y-m-d\TH:i:s", current_time( 'timestamp', 0 ) ) . $tempo . '</lastmod>' .
                "\n\t\t" . '<changefreq>daily</changefreq>' .
                "\n\t\t" . '<priority>1.0</priority>' .
                "\n\t" . '</url>' . "\n";
            foreach( $postsForSitemap as $post )
            {
                setup_postdata( $post);
                $postdate = explode( " ", $post->post_modified );
                $sitemap .= "\t" . '<url>' . "\n" .
                    "\t\t" . '<loc>' . get_permalink( $post->ID ) . '</loc>' .
                    "\n\t\t" . '<lastmod>' . $postdate[0] . 'T' . $postdate[1] . $tempo . '</lastmod>' .
                    "\n\t\t" . '<changefreq>Weekly</changefreq>' .
                    "\n\t\t" . '<priority>0.5</priority>' .
                    "\n\t" . '</url>' . "\n";
            }
            $sitemap .= '</urlset>';
            $fp = fopen( ABSPATH . "sitemap.xml", 'w' );
            fwrite( $fp, $sitemap );
            fclose( $fp );
        }
        //add_action( 'save_post', 'get_the_xml_sitemap' );
    }
    if (!function_exists('process_contact_form')) {
        function process_contact_form(){

            if (( isset( $_POST['sendMail'] ) && '1' == $_POST['sendMail'] )) {

                if ( $_POST['bodossaki-surname'] || ! wp_verify_nonce($_POST['bodossaki-name'], 'bodossaki-action') ) {
                //if ( $_POST['bodossaki-surname']) {

                    print 'Unauthorized';
                    exit;
                }
                else {
                    $myemail = "snomikos@converge.gr";
                    // $myemail = "info@hotelgoldenage.gr";
                    $contact_name      = (isset($_POST['fname']))? strip_tags(trim($_POST['fname'])) : null;
                    $contact_surname      = (isset($_POST['lname']))? strip_tags(trim($_POST['lname'])) : null;
                    $contact_email     = (isset($_POST['email']) && preg_match( '/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/', $_POST['email'] ))? trim($_POST['email']) : null;
                    $contact_telephone = (isset($_POST['phone']))? strip_tags(trim($_POST['phone'])) : null;
                    $contact_subject   = (isset($_POST['subject']))? strip_tags(trim($_POST['subject'])) : null;

                    $contact_message   = (isset($_POST['cmessage']))? htmlspecialchars(trim($_POST['cmessage']), ENT_NOQUOTES) : null;

                    $subject = 'Message from '.$contact_name.' | '.$contact_subject;
                    //$message ='[Name:'.$contact_name.'] [Email:'.$contact_email.']<h3>Message:</h3>'.$contact_message;
                    $message = '
                    <table>
						<tr><td><b>Contact form email</b></td><td><b>Bodossaki.gr</b></td></tr>
						<tr><td><hr></td><td><hr></td></tr>
                        <tr><td><b>Name:</b></td><td>'.$contact_name.'</td></tr>
                        <tr><td><b>SurName:</b></td><td>'.$contact_surname.'</td></tr>
                        <tr><td><b>Email:</b></td><td>'.$contact_email.'</td></tr>
                        <tr><td><b>Telephone:</b></td><td>'.$contact_telephone.'</td></tr>
                        <tr><td><b>Message:</b></td><td>'.$contact_message.'</td></tr>
                    </table>';

                    // To send HTML mail, the Content-type header must be set
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                    // Additional headers
                    //$headers .= 'To: Mary <mary@example.com>, Kelly <kelly@example.com>' . "\r\n";
                    $headers .= 'From: '.$contact_name.' <'.$contact_email.'>' . "\r\n";
                    //$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
                    //$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";


                    mail($myemail, $subject, $message , $headers,'-f'.$contact_email.'');
                }
                header('Location: '.get_site_url().'/');
                exit();
            }
        }
        add_action( 'init', 'process_contact_form' );
    }

    if (!function_exists('get_the_background_image')){
        function get_the_background_image($image = null, $title = nul){
            $output = '';
            if( !is_home() ){
                $output .= '
                <div id="full-scrn-slider-container">
                    <div id="full-scrn-slider">
                        <div class="full-scrn-slide full-scrn">
                            <img src="'.$image.'" alt="'.$title.'" />
                        </div>
                    </div>
                    <div id="full-scrn-bullets"></div>
                </div>';
            }
            return $output;
        }
    }
   /* **************************************************************************************
*****************************************************************************************
*****************************************************************************************
                                    OPTIONS FRAMEWORK FUNCTIONS
**************************************************************************************
**************************************************************************************
************************************************************************************** */

    if (!function_exists('options_get_the_value')) {
        function options_get_the_value($option_id = null) {

            $output = '';
            $options_prefix = get_the_prefix('options_');
            $option_id = $options_prefix.$option_id;
            if(of_get_option($option_id)){
                $output = of_get_option($option_id);
            }

            return $output;
        }
    }

/* **************************************************************************************
*****************************************************************************************
*****************************************************************************************
                                    WPML FUNCTIONS
**************************************************************************************
**************************************************************************************
************************************************************************************** */

define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
define('ICL_DONT_LOAD_NAVIGATION_CSS', true);

    if(!function_exists('get_the_language_code')) {
        /**
        * Get the current language code and filters out the default
        *
        * @param  string $current_language_code
        * @param  string $default_language_code
        * @return string
        */
        function get_the_language_code( $current_language_code, $default_language_code = 'el' ){

            $language_code = '';
            if( $current_language_code != $default_language_code ){
                $language_code = $current_language_code;
            }
            return $language_code;
        }
    }
    if(!function_exists('get_the_language_swither')) {
        /**
        * Creates language switcher
        *
        * @return string
        */
        function get_the_language_swither($type = null){
            $languages = icl_get_languages('skip_missing=1');
//             $languages = array(
//                             'en' => array(
//                                'id' => 1,
//                                'active' => 1,
//                                'native_name' => 'English',
//                                'missing' => 0,
//                                'translated_name' => 'English',
//                                'language_code' => 'en',
//                                'country_flag_url' => 'http://yourdomain/wpmlpath/res/flags/en.png',
//                                'url' => '#en'
//                             ),
//                             'fr' => array(
//                                'id' => 4,
//                                'active' => 0,
//                                'native_name' => 'Français',
//                                'missing' => 0,
//                                'translated_name' => 'French',
//                                'language_code' => 'fr',
//                                'country_flag_url' => 'http://yourdomain/wpmlpath/res/flags/fr.png',
//                                'url' => '#fr'
//                             ),
//                             'it' => array(
//                                'id' => 27,
//                                'active' => 0,
//                                'native_name' => 'Italiano',
//                                'missing' => 0,
//                                'translated_name' => 'Italian',
//                                'language_code' => 'it',
//                                'country_flag_url' => 'http://yourdomain/wpmlpath/res/flags/it.png',
//                                'url' => '#it'
//                             ),
//                             'el' => array(
//                                'id' => 29,
//                                'active' => 0,
//                                'native_name' => 'Ellinika',
//                                'missing' => 0,
//                                'translated_name' => 'Greek',
//                                'language_code' => 'el',
//                                'country_flag_url' => 'http://yourdomain/wpmlpath/res/flags/el.png',
//                                'url' => '#el'
//                             )
//
//                         );
            if( $type == 'links'){
                $output = '';
                foreach($languages as $l)
                {
                   // print_r($l);
                    // if($l['language_code']!='it')
                    // {
                        if(!$l['active']){
                            $langs[] = '<li><a href="'.$l['url'].'" >'.$l['language_code'].' </a></li>';
                                //$langs[] = '<a href="'.$l['url'].'" >'.$l['language_code'].'</a>';
                        }
                        else {
                            $langs[] = '<li><a href="'.$l['url'].'" style="font-weight: bold !important;">'.$l['language_code'].' </a></li>';
                            //$langs[] = '<a href="'.$l['url'].'"  style="color: #fcda1c;">'.$l['language_code'].'</a>';
                            //$langs[] = '<span style="opacity: .7;">'.$l['language_code'].'</span>';
                        }
                    // }
                }
                $output .= join('', $langs);
                return $output;
            }
            else {
                $output = '<select>';
                foreach($languages as $l)
                {
                    // print_r($l);
                    // if($l['language_code']!='it')
                    // {
                        if(!$l['active'])
                        {
                            if( ($l['language_code']=='fr') || ($l['language_code']=='it')){
                                $langs[] = '<option><a href="#" style="opacity: .150">'.$l['language_code'].' <img src="'.$l['country_flag_url'].'" style="border: 1px solid #c9c9c9; border-radius:2px; " /></a></option>';
                            }
                            else {
                                $langs[] = '<option><a href="'.$l['url'].'" >'.$l['language_code'].' <img src="'.$l['country_flag_url'].'" style="border: 1px solid #c9c9c9; border-radius:2px; opacity: .4" /></a></option>';
                                //$langs[] = '<a href="'.$l['url'].'" >'.$l['language_code'].'</a>';
                            }

                        }
                        else
                        {
                            $langs[] = '<option><a href="'.$l['url'].'" style="color: #fcda1c;">'.$l['language_code'].' <img src="'.$l['country_flag_url'].'" style="border: 1px solid #c9c9c9; border-radius:2px; opacity: .7" /></a></option>';
                            //$langs[] = '<a href="'.$l['url'].'"  style="color: #fcda1c;">'.$l['language_code'].'</a>';
                            //$langs[] = '<span style="opacity: .7;">'.$l['language_code'].'</span>';
                        }
                    // }
                }
                $output .= join('', $langs);
                $output .= '</select>';
                return $output;
            }

        }
    }
