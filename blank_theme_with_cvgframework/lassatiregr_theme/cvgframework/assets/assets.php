<?php
	/* ************************************************************************************** 
	Register Javascripts and CSS Files
	************************************************************************************** */
	if (!function_exists('cvgframework_load_scripts')) {
		function cvgframework_load_scripts() {
			if (!is_admin()) {
				$assets_path = get_template_directory_uri() .'/cvgframework/assets/';
				$infooter = true;
				$version = '1.0.3';
				//Register JS
					
					wp_register_script('cvg-js', $assets_path . 'js/cvg.js', 'jquery', $version, $infooter);
				//Enqueue JS
					wp_enqueue_script('jquery');					
					wp_enqueue_script('cvg-js');
					
				//Register CSS					             
					
					wp_register_style('cvg-css', $assets_path . 'css/cvg.css', '', $version, 'all');
				//Enqueue CSS
					
					wp_enqueue_style('cvg-css');
					
			}
		}
		add_action('init', 'cvgframework_load_scripts');
	}
	if (!function_exists('cvgframework_load_metatags')) {
		function cvgframework_load_metatags() {
			global $post;
			$metabox_prefix = get_the_prefix('metabox_', 'seo_general_box_');            
			$options_prefix = get_the_prefix('options_');            

			$description = ( ($seo_description = get_post_meta( $post->ID, $metabox_prefix.'description', true )) ? $seo_description : '' );
			$keywords = ( ($seo_keywords = get_post_meta( $post->ID, $metabox_prefix.'keywords', true )) ? implode(",", $seo_keywords) : '' );           

			$robots = '';
			// $mobile_web_app_capable = '';
			// $apple_mobile_web_app_capable = '';
			// $apple_mobile_web_app_status_bar_style = '';

			$apple_touch_icon_57_57 = '';
			$apple_touch_icon_60_60 = '';
			$apple_touch_icon_72_72 = '';
			$apple_touch_icon_76_76 = '';
			$apple_touch_icon_114_114 = '';
			$apple_touch_icon_120_120 = '';
			$apple_touch_icon_144_144 = '';
			$apple_touch_icon_152_152 = '';
			$apple_touch_icon_180_180 = '';

			$favicon_ico = '';
			$favicon_16_16 = options_get_the_value('favicon_16_16');
			$favicon_32_32 = '';
			$favicon_96_96 = '';
			$android_chrome_192_192 = '';

			$output = '';        


			if($description) 
			$output .= '<meta name="description" content="'.$description.'">';
			if($keywords) 
			$output .= '<meta name="keywords" content="'.$keywords.'">';
			if($robots) 
			$output .= '<meta name="robots" content="'.$robots.'">';
			/*

			<META NAME="subject" CONTENT="SH">
			<META NAME="Description" CONTENT="SDHSDH">               
			<META NAME="Keywords" CONTENT="SDH">
			<META NAME="Geography" CONTENT="SH">
			<META NAME="Language" CONTENT="SH">
			<META NAME="Copyright" CONTENT="SH">
			<META NAME="Designer" CONTENT="SH">
			<META NAME="Publisher" CONTENT="SH">
			<META NAME="distribution" CONTENT="Global">
			<META NAME="zipcode" CONTENT="SH">
			<META NAME="city" CONTENT="SH">
			<META NAME="country" CONTENT="SH">
			*/
			// $output .= '<meta name="author" content="">';
			// $output .= '<meta name="subject" content="">';
			// $output .= '<meta name="Description" content="">';
			// $output .= '<meta name="Classification" content="">';
			// $output .= '<meta name="Keywords" content="">';
			// $output .= '<meta name="Geography" content="">';
			// $output .= '<meta name="Language" content="">';
			// $output .= '<meta name="Copyright" content="">';
			// $output .= '<meta name="Designer" content="">';
			// $output .= '<meta name="Publisher" content="">';
			// $output .= '<meta name="distribution" content="Global">';
			// $output .= '<meta name="zipcode" content="">';
			// $output .= '<meta name="city" content="">';
			// $output .= '<meta name="country" content="">';
			// $output .= '<meta name="mobile-web-app-capable" content="yes">';
			// $output .= '<meta name="apple-mobile-web-app-capable" content="yes">';
			// $output .= '<meta name="apple-mobile-web-app-status-bar-style" content="default">';

			// $output .= '<link rel="apple-touch-icon" sizes="57x57"          href="/favicons/apple-touch-icon-57x57.png">';
			// $output .= '<link rel="apple-touch-icon" sizes="60x60"          href="/favicons/apple-touch-icon-60x60.png">';
			// $output .= '<link rel="apple-touch-icon" sizes="72x72"          href="/favicons/apple-touch-icon-72x72.png">';
			// $output .= '<link rel="apple-touch-icon" sizes="76x76"          href="/favicons/apple-touch-icon-76x76.png">';
			// $output .= '<link rel="apple-touch-icon" sizes="114x114"        href="/favicons/apple-touch-icon-114x114.png">';
			// $output .= '<link rel="apple-touch-icon" sizes="120x120"        href="/favicons/apple-touch-icon-120x120.png">';
			// $output .= '<link rel="apple-touch-icon" sizes="144x144"        href="/favicons/apple-touch-icon-144x144.png">';
			// $output .= '<link rel="apple-touch-icon" sizes="152x152"        href="/favicons/apple-touch-icon-152x152.png">';
			// $output .= '<link rel="apple-touch-icon" sizes="180x180"        href="/favicons/apple-touch-icon-180x180.png">';           
			// $output .= '<link rel="manifest"                                href="/favicons/manifest.json">';
			if($favicon_ico)                
			$output .= '<link rel="shortcut icon"                           href="'.$favicon_ico.'">';
			if($favicon_16_16)
			$output .= '<link rel="icon" type="image/png" sizes="16x16"     href="'.$favicon_16_16.'">';
			if($favicon_32_32)
			$output .= '<link rel="icon" type="image/png" sizes="32x32"     href="'.$favicon_32_32.'">';
			if($favicon_96_96)
			$output .= '<link rel="icon" type="image/png" sizes="96x96"     href="'.$favicon_96_96.'" >';
			if($android_chrome_192_192)
			$output .= '<link rel="icon" type="image/png" sizes="192x192"   href="'.$android_chrome_192_192.'">';

			// $output .= '<meta name="msapplication-TileColor"                content="#da532c">';
			// $output .= '<meta name="msapplication-TileImage"                content="/favicons/mstile-144x144.png">';
			// $output .= '<meta name="msapplication-config"                   content="/favicons/browserconfig.xml">';
			// $output .= '<meta name="theme-color"                            content="#ffffff">';

			// $output .= '<meta property="fb:page_id" content="">';
			// $output .= '<meta property="og:title" content="">';
			// $output .= '<meta property="og:image" content="">';
			// $output .= '<meta property="og:description" content="">';
			// $output .= '<meta property="og:url" content="">';
			// $output .= '<meta property="og:site_name" content="">';
			// $output .= '<meta property="og:type" content="website">';

			// $output .= '<meta name="twitter:card" content="summary">';
			// $output .= '<meta name="twitter:url" content="">';
			// $output .= '<meta name="twitter:title" content="">';
			// $output .= '<meta name="twitter:description" content="">';
			// $output .= '<meta name="twitter:image" content="">';
			// $output .= '<meta name="twitter:site" content="">';
			echo $output;
		}
		//add_action('wp_head', 'cvgframework_load_metatags');
    }
?>