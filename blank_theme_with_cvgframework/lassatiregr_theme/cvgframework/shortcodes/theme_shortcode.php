<?php
    /* ************************************************************************************** 
	General on specific theme
	************************************************************************************** */
	if (!function_exists('scode_info_box_func')) {
		function scode_info_box_func( $atts, $content = null ) {
			extract( shortcode_atts( array (		
				'header' => '',
				'link_text' => '',
				'link_url' => '',
				'target' => '_self',
			), $atts ) );                
			if($header) 
				$header = '<h4>'.$header.'</h4>';
			$output = '
			<div class="row">
				<div class="large-12 columns">
					<div class="cta-box bottom-line">
						<div class="cta-box-content full-width center">
							'.$header.'   
							'.do_shortcode($content).'
						</div>
						<div class="cta-box-button bottom large">
							<a href="'.$link_url.'" class="large round button" target="'.$target.'"><i class="icon ion-ios7-telephone"></i> '.$link_text.'</a>
						</div>
					</div>
				</div>
			</div>';
			return $output;
		}
	}
	/* ************************************************************************************** 
	Homepage Shortcodes
	************************************************************************************** */
	
	if (!function_exists('scode_international_func')) {
		function scode_international_func( $atts, $content = null ) {
			extract( shortcode_atts( array (		
				'heading' => 'Διεθνής Παρουσία',				
				'longer_text' => '',				
			), $atts ) );            
			
			$output = '
			<!-- start international-section -->
				<div id="international" class="container-fluid">
					<div class="row text-center">	
						
						<div class="international-section">					
							<div class="col-lg-12">
								<h3>'.$heading.'</h3>
								'.$longer_text.'								
							</div>
						</div>

					</div>					
			    </div>			  
		    <!-- /international-section -->';
			return $output;
		}
		add_shortcode( 'scode_international', 'scode_international_func' );
	}
	
	if (!function_exists('scode_contact_func')) {
		function scode_contact_func( $atts, $content = null ) {
			extract( shortcode_atts( array (		
				'heading' => '',
				'longer_text' => '',
				'details_name' => '',
				'details_address' => '',
				'details_email' => '',
				'details_phone' => '',
				'form_name' => '',
				'form_email' => '',
				'form_comment' => '',
				'form_submit' => '',
			), $atts ) );                
			
			$output = '
			<!-- start contact-section -->
				<div id="contact" class="container-fluid">
					<div class="row text-center">
						<div class="contact-section">					
							<!--h3>'.$heading.'</h3-->
							
							<div class="col-lg-6">							
								<p class="contact-text">
									'.$longer_text.'
								</p>								
							</div>							
							<div class="col-lg-6">
								<div class="contact-form">
									<form method="post" action="" name="zero-candies-contact-form">	
										'.wp_nonce_field( 'zero-candies-action', 'zero-candies-name').'	
										<input type="hidden" name="zero-contact-form-submission" value="1" />						
										<div class="form-group row">										
											<div class="col-sm-6">
												<input type="text" name="zero-name" class="form-control" id="inputName" placeholder="'.$form_name.'" />
												<input type="text" name="zero-surname" style="display:none" class="form-control" id="inputName" placeholder="" value="" />
											</div>
																				
											<div class="col-sm-6">
												<input type="email" name="zero-email" class="form-control" id="inputEmail3" placeholder="'.$form_email.'">
											</div>
										</div>
									  	<div class="form-group row">								    	
											<div class="col-sm-12">
												<textarea class="form-control"  name="zero-comment" id="inputTextarea" placeholder="'.$form_comment.'"></textarea>
											</div>
									  	</div>
										<div class="form-group row">
											<div class="col-sm-12">
												<input type="submit" name="zero-submit" class="btn btn-candies col-sm-12" value="'.$form_submit.'" />
											</div>
										</div>
									</form>
								</div>
							</div>
							<!--div class="col-lg-12">
								<p class="contact-details">
									<span class="details-left">'.$details_name.' '.$details_address.'</span> <span class="details-right"> E: '.$details_email.' Τ: '.$details_phone.'</span>
								</p>
							</div-->

						</div>					
					</div>					
			    </div>			  
		    <!-- /contact-section -->';

			return $output;
			//die(print_r($extract);
		}
		add_shortcode( 'scode_contact', 'scode_contact_func' );
	}
