 
<?php 
/*
Template Name: Homepage template
*/
get_header();
	$output = '';
	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
			//post data
				$single_id = get_the_ID();
				$single_title = get_the_title();
				$single_content = get_the_content();
				$single_permalink = get_the_permalink();
				$single_featured_image = wp_get_attachment_url( get_post_thumbnail_id($single_id) );
			//metabox data	
				//$metabox_prefix = get_the_prefix('metabox_', 'site_settings_header_box_');
				//$site_settings_image_right = wp_get_attachment_url(get_post_meta( get_the_id_of_all_site_settings(), $metabox_prefix.'image_right', true ));	

				$args = array( 'post_type' => 'post', 'posts_per_page' => '5', 'post_parent' => 0, 'order' => 'DESC',  );
				$loop = new WP_Query( $args );
				$post_items = '';
				if( $loop->have_posts() ){
					//die('<pre>'.$loop->found_posts.'</pre>');
					$post_items .= '
					<div class="home-left">
						<h2>'.__( 'Posts', get_theme_text_domain() ).'</h2>
						<ul>
					';
							while ( $loop->have_posts() )
							{
								$loop->the_post();
								//post data    
									$this_id = get_the_ID();
									$this_title = get_the_title();
									$this_content = get_the_content_with_formatting();
									//$this_projects_categories = get_terms(array('projects-category'));
									
									$this_permalink = get_the_permalink(); 
									$this_post_date = get_the_date();
									$this_featured_image = wp_get_attachment_url( get_post_thumbnail_id($this_id) );
									$post_items .= '
									<li>
										<a href="'.$this_permalink.'">
											<h4>'.$this_title.'</h4>
											<p><span>'.$this_post_date.'</span></p>
										</a>
									</li>							
									';
							}
					$post_items .= '
						</ul>							
					</div>
					';
					
				}
				wp_reset_postdata();
				
				$output .= '				
				<div>
					<img src="'.$single_featured_image.'" alt="'.$single_title.'" />
				</div>
				<div>					
					'.$post_items.'
					<div>
						<h1>'.$single_title.'</h1>
						'.$single_content.'
					</div>
				</div>
				';
							
		endwhile;
	endif;
	echo $output;
get_footer(); 
?>
