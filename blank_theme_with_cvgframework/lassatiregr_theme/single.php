<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package LassatireGr_Theme
 */
get_header();
	$output = '';
	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
			//post data
				$single_id = get_the_ID();
				$single_title = get_the_title();
				$single_content = get_the_content();
				$single_permalink = get_the_permalink();
				$single_featured_image = wp_get_attachment_url( get_post_thumbnail_id($single_id) );
					
				$output .= '
				<div>
					<img src="'.$single_featured_image.'" alt="'.$single_title.'" />						
				</div>
				<div>
					<div>'.get_the_breadcrumb1().'</div>					
					<h1>'.$single_title.'</h1>
					'.$single_content.'
				</div>
				';							
		endwhile;
	endif;
	echo $output;
get_footer();