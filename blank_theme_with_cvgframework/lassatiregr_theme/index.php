<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package LassatireGr_Theme
 */

get_header();
	$output = '';
	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
			//post data
				$single_id = get_the_ID();
				$single_title = get_the_title();
				$single_content = get_the_content();
				$single_permalink = get_the_permalink();
				$single_featured_image = wp_get_attachment_url( get_post_thumbnail_id($single_id) );
			//metabox data				
				
				
				$output .= '
				<div>						
					<h1>'.$single_title.'</h1>
					'.$single_content.'
				</div>					
				';
							
		endwhile;
	endif;
	echo $output;
get_footer(); 
