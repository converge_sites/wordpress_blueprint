<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package LassatireGr_Theme
 */
get_header();
	$output = '';
	if ( have_posts() ) :
		$output .= '<header class="page-header">'.get_the_archive_title( '<h1 class="page-title">', '</h1>' ).'<br/>'.get_the_archive_description( '<div class="taxonomy-description">', '</div>' ).'</header>';
		while ( have_posts() ) : the_post();
			//post data
				$single_id = get_the_ID();
				$single_title = get_the_title();
				$single_content = get_the_content();
				$single_permalink = get_the_permalink();
				$single_featured_image = wp_get_attachment_url( get_post_thumbnail_id($single_id) );
			//metabox data				
				
				
				$output .= '
				<div>						
					<h1>'.$single_title.'</h1>
					'.$single_content.'
				</div>					
				';
							
		endwhile;
		$output .= get_the_posts_pagination();
	endif;
	echo $output;
get_footer();