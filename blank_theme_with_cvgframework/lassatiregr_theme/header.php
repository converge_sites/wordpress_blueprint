<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package LassatireGr_Theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<title><?php single_post_title( bloginfo("name").' | ' ); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div class="main-container">
		<div class="main_menu_el">
			<?php
				$menu_name = 'main_menu_'.ICL_LANGUAGE_CODE;
				$extra_items  = '
				<li class="extra-li"></li>
				';
				$top_header_menu = array(
					'theme_location'  => '',
					'menu'            => $menu_name,
					'container'       => '',
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => 'main-menu',
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					//%1$s is expanded to the value of the 'menu_id' parameter, %2$s is expanded to the value of the 'menu_class' parameter, and %3$s is expanded to the value of the list items
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s'.$extra_items.'</ul>',
					'depth'           => 0,
					'walker'          => ''
				);
				wp_nav_menu( $top_header_menu );
			?>
		</div><!-- /nav-collapse -->